#!usr/bin/perl
use strict;

my $puntuaciones = {
    '1' => 1,
    '2' => 2,
    '3' => 3,
    '4' => 4,
    '5' => 5,
    '6' => 6,
    'escalera' => 25,
    'full' => 30,
    'poker' => 40,
    'generala' => 50,
    'generala2' => 100,
}

my $tabla = {
    '1' => undef,
    '2' => undef,
    '3' => undef,
    '4' => undef,
    '5' => undef,
    '6' => undef,
    'escalera' => undef,
    'full' => undef,
    'poker' => undef,
    'generala' => undef,
    'generala2' => undef,
};

juego();

sub juego {
    
    for (1..12){
        turno();
    }

}

sub turno {
    my $dados = {
        '1' => undef,
        '2' => undef,
        '3' => undef,
        '4' => undef,
        '5' => undef,
    };

    my $count = {
        '1' => 0,
        '2' => 0,
        '3' => 0,
        '4' => 0,
        '5' => 0,
        '6' => 0,
    }

    foreach my $tirada (1..3){
        my $dados_a_tirar = elegir($dados) if $_ != 1;
        my $nuevos = tirar($dados_a_tirar,$tirada);
        #rearmo los dados
        foreach my $num ( @$nuevos ){
            $dados->{$num} = $nuevos->{$num};
        }
    }

}

sub tirar {
    my ($dados,$tirada) = shift;

    foreach my $dado ( $dados ){
        $count->{$dado} = $count->{$dado} +1;
    }

    if( $tirada == 1 ){
        my $ordenados = [ map { $count->{$_} } grep { $_ != 0 } sort { $b->{$_} <=> $a->{$_} } keys %$count ]; # array de counts ordenado
        my $cant_max_repetidos = $ordenados->[0] == 5;
        if ( $ordenados->[0] == 5 ){
            generala();
        }elsif( $ordenados->[0] == 4 ){
            poker();
        }elsif( $ordenados->[0] == 3 && $ordenados->[1] == 2 ){
            full();
        }elsif( scalar (grep { $_ != 0 } @$ordenados) == 5 && ($count->{1} == 0 || $count->{6} == 0) ){
            escalera();
        }

    }elsif( $tirada == 2 ){

    }else{ #tirada 3

    }

}

sub elegir {
    my $dados;



}