#!/usr/bin/ruby
class Juego
    def initialize(jugador)
        @jugador = jugador
        @tabla = Tabla.new()
        @puntuacion = Puntuacion.new()
        this.jugar()
    end

    def jugar
        [1..12].each |numero_turno| { 
            turno = Turno.new( numero_turno, @jugador )
            turno_resultado = turno.jugar;
            @tabla.set_puntuacion( turno_resultado );
        }
    end

end

class Turno
    def initialize(numero,jugador)
        @numero = numero
        @jugador = jugador
    end

    def get_numero
        return @numero
    end
    def get_jugador
        return @jugador
    end

    def jugar
        [1..3].each |numero_tirada| { 
            @dados = Tirada.new(numero_tirada,@jugador)
            accion = @jugador.get_proxima_accion( dados ) # tirar(dados)|anotar(set)
            if ( accion.nombre == 'tirar' ){
                dados = accion.get_dados
            }elsif( accion.nombre == 'anotar' )
                dados = accion.get_set
            }
        }
        @conjunto = 
        return TurnoResultado.new( @conjunto, @puntuacion )
    end
end

class Tirada
    def initialize(number)
        @player_name = player_name
    end

    def player_name
        return @player_name
    end
end

class TurnoResultado
    def initialize(number)
        @conjunto = conjunto
        @puntuacion = puntuacion
    end

    def get_conjunto
        return @conjunto
    end
    def get_puntuacion
        return @puntuacion
    end
end

class Conjunto
    def initialize(number)
        @number = number
        @value = value
    end

    def player_name
        return @player_name
    end
end

class Dado
    def initialize(number)
        @number = number
        @value = value
    end

    def player_name
        return @player_name
    end
end

class Puntuacion
    def initialize()
        @puntuaciones = {
            escalera: 25
            full: 30
            poker: 40
            generala: 50 
            generala2: 100
        }
    end

    def get_puntuacion(set)
        return @puntuaciones[set]
    end
end

class Tabla
    def initialize(score)
        @name = name
        @score = score
    end

    def player_name
        return @player_name
    end
end

class Set
    def initialize(nombre)
        @nombre = nombre
        @puntuacion = @puntuacion
    end

    def player_name
        return @player_name
    end
end

class Jugador
    def initialize(nombre, tipo)
        @nombre = nombre
        @tipo = tipo
    end

    def get_nombre
        return @nombre
    end
    def get_tipo
        return @tipo
    end
end

class TipoJugador
    def initialize(tipo)
        @tipo = tipo
    end

    def get_tipo
        return @tipo
    end
end

class Accion
    def initialize(nombre)
        @nombre = nombre
    end

    def get_nombre
        return @nombre
    end
end

# Start Game
Juego.new();